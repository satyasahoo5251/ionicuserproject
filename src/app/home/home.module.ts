import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { IonicModule } from '@ionic/angular';
import { homePageRoutingModule } from './home-routing.module';

import { homePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    homePageRoutingModule,
    HttpClientModule
  ],
  declarations: [homePage]
})
export class homePageModule {}
