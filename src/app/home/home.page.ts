import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from '../modal/modal.component';
import { Vibration } from '@ionic-native/vibration/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class homePage implements OnInit {
  public home: string;
  page = 1;
  resultsCount = 6;
  totalPages = 2;
  masEdit = false;
  data = [];
  result= [];
  imgUrl = [];
  // noData = alert("no data found");

  constructor(private activatedRoute: ActivatedRoute,private alertCtrl: AlertController, private http: HttpClient, private modalController: ModalController,private vibration: Vibration) { }
  ngOnInit() {
    this.home = this.activatedRoute.snapshot.paramMap.get('id');
    this.loadData();
  }

  loadData() {
      this.http.get(`https://reqres.in/api/users?page=${this.page}&results=${this.resultsCount}`).subscribe ((res:any) => {
      console.log("res_" , res.data);
      this.result = res.data;
    }, error => {
      return this.presentAlert();
    });
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      // subHeader: 'Subtitle',
      message: 'Internet Not evalable',
      buttons: ['OK']
    });

    await alert.present();
  }
  // nextPage() {
  //   this.page++ ;
  //   this.loadData();
  // }

  // prevPage() {
  //   this.page--;
  //   this.loadData();
  // }

  async openModal(item) {
    const modal = await this.modalController.create({
      component: ModalComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        id: item.id,
        firstName: item.first_name,
        lastName: item.last_name,
        email: item.email,
        avatar: item.avatar
      }
    });
    return await modal.present();
  }

  goFirst() {
    this.page = 1;
    this.loadData();
  }

  goLast() {
    this.page = this.totalPages;
    this.loadData();
  }

  filterData(event:any) {
    const res = event.target.value;
    // console.log(res);
    if(res && res.trim() != ''){
      this.result = this.result.filter((item) => {
        return (item.first_name.toLowerCase().indexOf(res.toLowerCase()) >- 1);
      })
    } if(res.trim() == ''){
      this.loadData();
    }
  }

}
