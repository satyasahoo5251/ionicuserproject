import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { Camera } from '@ionic-native/camera/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { homePageModule } from './home/home.module';
import { ModalComponent } from './modal/modal.component';
import { AlertController } from '@ionic/angular';
@NgModule({
  declarations: [AppComponent,ModalComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    homePageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,,
    Camera,
    Vibration,
    AlertController,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
